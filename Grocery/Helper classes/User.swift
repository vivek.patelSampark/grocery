//
//  User.swift
//  Glam360
//
//  Created by Night Reaper on 30/06/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import EZSwiftExtensions


prefix operator ¿
prefix func ¿(value : String?) -> String {
    return value.unwrap()
}



class User : NSObject {
 
    override init() {
        super.init()
    }
    
}


protocol StringType { var get: String { get } }
extension String: StringType { var get: String { return self } }

extension Optional where Wrapped: StringType {
    func unwrap() -> String {
        return self?.get ?? ""
    }
}

extension String {
    var doubleValue: Double {
        return (self as NSString).doubleValue
    }
}




