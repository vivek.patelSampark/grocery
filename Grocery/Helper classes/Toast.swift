//
//  Toast.swift
//  OutOfOffice
//
//  Created by Sierra 4 on 31/05/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import SwiftMessages

class Toast {
    
    class func show(text : String?,type : Theme,layout : MessageView.Layout? , direction : SwiftMessages.PresentationStyle? , iconImage : UIImage? = nil){
        let view = MessageView.viewFromNib(layout: .messageView)
        
       
        // Theme message elements with the warning style.
        view.configureTheme(type)
        view.configureTheme(backgroundColor: UIColor.green, foregroundColor: UIColor.white)
        view.titleLabel?.font = UIFont(name: "Gotham-Book", size: 15.0)
        view.bodyLabel?.font = UIFont(name: "Gotham-Book", size: 13.0)
        view.configureIcon(withSize: CGSize(width:20.0 , height: 20.0), contentMode: .center)
        // Add a drop shadow.
        view.configureDropShadow()
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        view.button?.isHidden = true
        view.configureContent(title: type.string(), body: text ?? "", iconImage: iconImage, iconText: nil, buttonImage: nil, buttonTitle: "OK") { (button) in
            SwiftMessages.hide()
        }
        SwiftMessages.defaultConfig.presentationStyle = direction ?? .top
   
        SwiftMessages.defaultConfig.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        
        // Disable the default auto-hiding behavior.
        SwiftMessages.defaultConfig.duration = .seconds(seconds: 1.0)
        
        // Dim the background like a popover view. Hide when the background is tapped.
        //SwiftMessages.defaultConfig.dimMode = .gray(interactive: true)
        SwiftMessages.defaultConfig.dimMode = .none
        
        // Disable the interactive pan-to-hide gesture.
        SwiftMessages.defaultConfig.interactiveHide = true
        
        // Specify a status bar style to if the message is displayed directly under the status bar.
        SwiftMessages.defaultConfig.preferredStatusBarStyle = .lightContent
        // Show message with default config.
      //  SwiftMessages.show(config: Config, view: view)
        SwiftMessages.show(view: view)
        
        // Customize config using the default as a base.
       // var config = SwiftMessages.defaultConfig
      //  config.duration = .forever
        
        // Show the message.
       // SwiftMessages.show(config: config, view: view)
    }
    
    
    
//        class func show(text : String?,type : Theme,layout : MessageView.Layout? , direction : SwiftMessages.PresentationStyle? , iconImage : UIImage? = nil){
//            
//            var warningConfig = SwiftMessages.defaultConfig
//            warningConfig.presentationStyle = .top
//            warningConfig.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
//            warningConfig.duration = .seconds(seconds: 1.0)
//            let warning = MessageView.viewFromNib(layout: .MessageView)
//            warning.button?.isHidden = true
//            warning.configureDropShadow()
//            warning.titleLabel?.font = UIFont(name: "Lato", size: 16.0)
//            warning.bodyLabel?.font = UIFont(name: "Lato-Bold", size: 14.0)
//            warning.configureContent(title: type.string(), body: /text)
//            switch type {
//            case .error:
//                warning.configureTheme(.error)
//                warning.backgroundView.backgroundColor = Colors.appTheme.color()
//            case .info:
//                warning.configureTheme(.warning)
//                warning.backgroundView.backgroundColor = Colors.appTheme.color()
//            case .success:
//                warning.configureTheme(.success)
//                warning.backgroundView.backgroundColor = Colors.appTheme.color()
//            case .info : break
//            default : break
//            }
//            SwiftMessages.show(config: warningConfig, view: warning)
//       
//    }
}

extension Theme {
    
    func string() -> String{
        switch self {
        case .success:
            return "Success"
        case .error:
            return "Error"
        case .warning:
            return "Warning"
        case .info:
            return ""
        }
    }
    
}

