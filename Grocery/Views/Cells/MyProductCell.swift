//
//  MyProductCell.swift
//  Grocery
//
//  Created by vivek patel on 25/09/18.
//  Copyright © 2018 vivek patel. All rights reserved.
//

import UIKit

class MyProductCell: UITableViewCell {

    @IBOutlet weak var collectionview: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionview.dataSource = self
      //  collectionview.delegate = self
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}


extension MyProductCell : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionProductCell", for: indexPath) as?  CollectionProductCell   else { return UICollectionViewCell() }

   // collectionView.su
    
        return cell
    }

}




