//
//  LoginEndPoint.swift
//  Tracking
//
//  Created by vivek patel on 19/04/18.
//  Copyright © 2018 vivek patel. All rights reserved.
//

import UIKit
import Foundation
import EZSwiftExtensions
import Alamofire

enum LoginEndPoint {
    case getOTP(mobile : String? , message : String?)
    case login(username : String? , password : String?)
}


extension LoginEndPoint : Router {
    
    var route : String  {
        switch self {
            
        case .getOTP(_):
            return APIConstants.getOTP
            
        case .login(_):
             return APIConstants.login
        }

        
    }
    
    var parameters: OptionalDictionary{
        return format()
    }
    
    func format() -> OptionalDictionary {
        
        switch self {
       
            
        case .getOTP(let mobile,let  message):
            let dict = Parameters.getOTP.map(values: [mobile , message])
            return dict
            
        case .login(let username,let password):
            let dict = Parameters.login.map(values: [username,password])
            return dict
            
        }
        
        
    }
    
    var method : Alamofire.HTTPMethod {
        switch self {
            
        default : return  .post
        }
        
    }
    
    var baseURL: String{
        
        switch self {
        case .getOTP(_):
            return APIConstants.basePathOTP
        default:
            return APIConstants.basePath
        }
    }
}
