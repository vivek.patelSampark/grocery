
//  APIManager.swift
//  ChillaxPartner
//
//  Created by Sierra 4 on 09/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import EZSwiftExtensions
import NVActivityIndicatorView


class APIManager : UIViewController{
    
    typealias Completion = (Response) -> ()
    static let shared = APIManager()
    private lazy var httpClient : HTTPClient = HTTPClient()
    
    func request(with api : Router , isLoaderNeeded : Bool? = true ,completion : @escaping Completion )  {
        
        if isConnectedToNetwork(){
            
            if isLoaderNeeded ?? true  {
                // CommonFunctions.shared.showLoader(Colors.darkOrange)
                showLoader()
            }
            
            
            httpClient.postRequest(withApi: api, success: {[weak self] (data) in
                self?.hideLoader()
                
                guard let response = data else {
                    completion(Response.failure(.none))
                    return
                }
                
                let json = JSON(response)
                print("json" , json)
                
            
                let responseType = Validate(rawValue: json[APIConstants.status].stringValue) ?? .failure
                
                if responseType == Validate.success {
                    
                    let object : AnyObject?
                    object = api.handle(parameters: json)
                    completion(Response.success(object))
                    return
                }
                else{
                 completion(Response.failure("failure"))

                }
                }, failure: {[weak self] (message) in
                    self?.hideLoader()
                    completion(Response.failure(message))
            })
        }else{
          //  Toast.show(text: " Please enable your internet connection " , type: .warning, layout:.CardView, direction: .top , iconImage : #imageLiteral(resourceName: "iconn"))
        }
        
    }
    
  
    func request (withImages api : Router , images : [UIImage]?  , completion : @escaping Completion )  {
        
        if isConnectedToNetwork(){
            
            if isLoaderNeeded(api: api) {
                // CommonFunctions.shared.showLoader(Colors.darkOrange)
                showLoader()
            }
            httpClient.postRequestWithImages(withApi: api, images: images, success: {[weak self] (data) in
                self?.hideLoader()
                guard let response = data else {
                    completion(Response.failure(.none))
                    return
                }
                let json = JSON(response)
                print(json)
                
                let responseType = Validate(rawValue: json[APIConstants.status].stringValue) ?? .failure
                switch responseType {
                case .success:
                    let object : AnyObject?
                    object = api.handle(parameters: json)
                    completion(Response.success(object))
                    
                case .failure(_):
                    completion(Response.failure(json[APIConstants.message].stringValue))
                    
                default : break
                }
                }, failure: {[weak self] (message) in
                    self?.hideLoader()
                    completion(Response.failure(message))
            })
        }else{
          //  Toast.show(text: " Please enable your internet connection " , type: .warning, layout:.CardView, direction: .top , iconImage : #imageLiteral(resourceName: "iconn"))
        }
        
    }
    

    func showLoader(){
        
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
        (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController?.startAnimating(CGSize(width: 80,height: 60), message: "", type: .ballScale, color: UIColor(hexString: "0x043A4E"), padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 0)
    }
    func hideLoader(){
        
        (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController?.stopAnimating()
    }
    
    func isLoaderNeeded(api : Router) -> Bool {
        switch api.route {
        default: return true
        }
    }
}

extension UIViewController : NVActivityIndicatorViewable { }


func isConnectedToNetwork() -> Bool {
    guard let reachability = Alamofire.NetworkReachabilityManager()?.isReachable else { return false }
    return reachability ? true: false
}
