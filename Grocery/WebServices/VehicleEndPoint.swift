//
//  VehicleEndPoint.swift
//  Tracking
//
//  Created by vivek patel on 23/04/18.
//  Copyright © 2018 vivek patel. All rights reserved.
//

import UIKit
import Foundation
import EZSwiftExtensions
import Alamofire

enum VehicleEndPoint {
    case listVehicle()
    case VehicleDetails(terminalID : String?)
}


extension VehicleEndPoint : Router {
    
    var route : String  {
        switch self {
            
        case .listVehicle(_):
            return APIConstants.vehicleList
            
        case .VehicleDetails(let terminalID) :
            return APIConstants.vehicleDetail + "/" + (terminalID ?? "")
            
        }
    }
    
    var parameters: OptionalDictionary{
        return format()
    }
    
    func format() -> OptionalDictionary {
        
        switch self {
        
        case .listVehicle() , .VehicleDetails(_):
            let dict = Parameters.listVehicles.map(values: [])
            return dict
            
        }
    }
    
    var method : Alamofire.HTTPMethod {
        switch self {
            
        default : return  .post
        }
        
    }
    
    var baseURL: String{
        
        return APIConstants.basePath
    }
}

