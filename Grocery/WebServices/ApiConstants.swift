//
//  File.swift
//  truckTrack
//
//  Created by rishabh.yadav on 3/15/18.
//  Copyright © 2018 Sampark Softwares. All rights reserved.
//

import Foundation

internal struct APIConstants {

    static let basePathOTP = "http://125.63.102.130:9099/"
   // static let basePath = "http://192.168.10.156:8083/"
    static let basePath = "http://195.201.29.93:8080/"
    static let status = "status"
    static let message = "message"
    static let responseType = "responseType"
    static let error = "error"
    static let dataNotFound = "DATA_NOT_FOUND"
    static let flatDetails = "mobile/flatDetails"
    static let getOTP = "SMSWeb/outbound.do"
    static let vehicleList = "Tracing/web/mobile/list/vehicless"
    static let vehicleDetail = "Tracing/web/mobile/latlng"
    static let login = "Tracing/web/mobile/login"
    
}


enum Keys : String {
    
    case appVersion = "appVersion"
    case deviceToken = "deviceToken"
    case deviceType = "deviceType"
    
    case societyID = "SocietyID"
    case flatNo = "FlatNo"
    
    case mobile = "mobile"
    case message = "message"
    case terminalID = "vdsf"
    case username = "username"
    case password = "password"
    
}

enum Validate: String {
    
    case none
    case success = "200"
    case failure = "400"
    case invalidAccessToken = "401"
    case blocked = "403"
    case dataNotFound = "404"
    case internalServerError = "500"
    
    
    func map(response message: String?) -> String? {
        switch self {
        case .success:
            return message
        case .failure:
            return message
        case .invalidAccessToken:
            return message
        case .dataNotFound:
            return message
        case .internalServerError:
            return message
        default:
            return nil
        }
    }
}

enum Response {
    case success(AnyObject?)
    //    case failure(Validate)
    case failure(String?)
}

typealias OptionalDictionary = [String : String]?

struct Parameters {
    
    static let flatDetails : [Keys] = [.societyID , .flatNo]
    static let getOTP : [Keys] = [.mobile , .message]
    static let login : [Keys] = [.username , .password]
    static let listVehicles : [Keys] = []
    
}

