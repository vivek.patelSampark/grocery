//
//  MyProductController.swift
//  Grocery
//
//  Created by vivek patel on 20/09/18.
//  Copyright © 2018 vivek patel. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class MyProductController: UIViewController , IndicatorInfoProvider {

    @IBOutlet weak var tableview: UITableView!{
        didSet{
            tableview.dataSource = dataSource
            tableview.delegate = dataSource
        }
    }
    
    var items : Array<Any>?
    var dataSource : TableViewDataSource?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        // Do any additional setup after loading the view.
    }
    
    
    func configureTableView(){
        // checkNilData(arr: items as Array<AnyObject>?, tableView: tableView, contentMode: .center)
        items = ["a" , "b" , "a" , "b" , "a" , "b" , "a" , "b" ,"a" , "b" ,"a" , "b" ,"a" , "b" , "a" , "b" , "a" , "b" ,"a" , "b" ,"a" , "b" ,"a" , "b" ,"a" , "b" ,"a" , "b"]
        
        
        if /items?.isEmpty {
            //   lblNoData.isHidden = false
        } else {
            //  lblNoData.isHidden = true
        }
        
        dataSource = TableViewDataSource(items: items, height: UIScreen.main.bounds.height / 2, tableView: tableview, cellIdentifier: "MyProductCell", configureCellBlock: { (cell, item, indexPath) in
            
            //  (cell as? MyCustomerCell)?.item = (item as? AllOrderResModel)
            
            
        })
        
        tableview.dataSource = dataSource
        tableview.delegate = dataSource
        tableview.reloadData()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        
        return IndicatorInfo(title: "My products")
    }

}
