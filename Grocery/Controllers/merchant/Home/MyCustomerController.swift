//
//  MyCustomerController.swift
//  Grocery
//
//  Created by vivek patel on 20/09/18.
//  Copyright © 2018 vivek patel. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class MyCustomerController: UIViewController , IndicatorInfoProvider {

    // Mark : Outelts
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.dataSource = dataSource
            tableView.delegate = dataSource
        }
    }
    var items : Array<Any>?
    var dataSource : TableViewDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }
    
    
    func configureTableView(){
        // checkNilData(arr: items as Array<AnyObject>?, tableView: tableView, contentMode: .center)
        items = ["a" , "b" , "a" , "b" , "a" , "b" , "a" , "b" ,"a" , "b" ,"a" , "b" ,"a" , "b" , "a" , "b" , "a" , "b" ,"a" , "b" ,"a" , "b" ,"a" , "b" ,"a" , "b" ,"a" , "b"]
        
        
        if /items?.isEmpty {
         //   lblNoData.isHidden = false
        } else {
          //  lblNoData.isHidden = true
        }
        
        dataSource = TableViewDataSource(items: items, height: UITableViewAutomaticDimension, tableView: tableView, cellIdentifier: "MyCustomerCell", configureCellBlock: { (cell, item, indexPath) in
            
          //  (cell as? MyCustomerCell)?.item = (item as? AllOrderResModel)
            
        })
        
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        tableView.reloadData()
    }

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        
        return IndicatorInfo(title: "My customers")
    }
}
