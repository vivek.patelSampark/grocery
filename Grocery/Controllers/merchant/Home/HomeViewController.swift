//
//  HomeViewController.swift
//  Grocery
//
//  Created by vivek patel on 20/09/18.
//  Copyright © 2018 vivek patel. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class HomeViewController: ButtonBarPagerTabStripViewController {

    var vc1 =  MyCustomerController()
    var vc2 =  MyProductController()
    var vc3 =  OrdersController()
    
    override func viewDidLoad() {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Home", bundle:nil)
        
        vc1 = storyBoard.instantiateViewController(withIdentifier: "MyCustomerController") as! MyCustomerController
        vc2 = storyBoard.instantiateViewController(withIdentifier: "MyProductController") as! MyProductController
        vc3 = storyBoard.instantiateViewController(withIdentifier: "OrdersController") as! OrdersController
        
        pageVCSetup()
        super.viewDidLoad()
    }
    
    func pageVCSetup() {
        
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        //  settings.style.buttonBarLeftContentInset = 64
        //   settings.style.buttonBarRightContentInset = 64
        settings.style.buttonBarItemLeftRightMargin = 16.0
        settings.style.buttonBarHeight = 40.0
        //  settings.style.buttonBarMinimumLineSpacing = 0
        //  settings.style.buttonBarMinimumInteritemSpacing = 0
        //  settings.style.selectedBarVerticalAlignment = .bottom
        
        
        settings.style.buttonBarItemFont = UIFont(name: "ProximaNova-Bold", size: 16.0)!
        settings.style.selectedBarHeight  = 2.0
        settings.style.selectedBarBackgroundColor = UIColor.black
        settings.style.buttonBarItemTitleColor = UIColor.black
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.lightGray
            newCell?.label.textColor = UIColor.black
        }
        //  settings.style.buttonBarItemsShouldFillAvailableWidth = false
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        return [vc1 , vc2 , vc3]
    }

}
