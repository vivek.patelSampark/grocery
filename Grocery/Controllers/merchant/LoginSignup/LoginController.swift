//
//  LoginController.swift
//  Tracking
//
//  Created by vivek patel on 20/04/18.
//  Copyright © 2018 vivek patel. All rights reserved.
//

import UIKit
import Material

class LoginController: UIViewController {

    
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
    }
    
    func clearData(){
        tfUsername.text = ""
        tfPassword.text = ""
    }
    

//    func webServices(){
//
//        guard let username = tfUsername.text , username.count > 4 else {
//            Toast.show(text: "please enter a valid username.", type: .warning, layout: .messageView, direction: .top)
//            return
//        }
//
//        guard let password = tfPassword.text , password.count > 4 else {
//             Toast.show(text: "please enter a valid password.", type: .warning, layout: .messageView, direction: .top)
//            return
//        }
//
//        APIManager.shared.request(with: LoginEndPoint.login(username: tfUsername.text, password: tfPassword.text)) { [unowned self] (response) in
//            switch response {
//            case .success(_) :
//                self.goToDashboard()
//                break
//            case .failure(_):
//               Toast.show(text: "Wrong username or password", type: .warning, layout: .messageView, direction: .top)
//                break
//            }
//        }
//    }
    
    func goToDashboard(){
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController else {return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnLogin(_ sender: UIButton) {
        self.goToDashboard()
       // webServices()
    }
    


}
