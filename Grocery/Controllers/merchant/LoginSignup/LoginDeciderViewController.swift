//
//  LoginDeciderViewController.swift
//  Tracking
//
//  Created by vivek patel on 18/04/18.
//  Copyright © 2018 vivek patel. All rights reserved.
//

import UIKit
import Material
import EZSwiftExtensions

class LoginDeciderViewController: UIViewController {

    
    @IBOutlet weak var tfMobileNo: TextField!
    @IBOutlet weak var tfOTP: TextField!
    @IBOutlet weak var tfFirstname: TextField!
    @IBOutlet weak var tfLastName: TextField!
    @IBOutlet weak var tfUserName: TextField!
    @IBOutlet weak var tfEmail: TextField!
    @IBOutlet weak var tfPassword: TextField!
    @IBOutlet weak var tfConfirmPassword: TextField!
    
    
    //buttons
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
    }

    
    
    
    func addDoneButtonOnKeyboard()
    {
        
        let viewInput = UIView(frame: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width:ScreenSize.SCREEN_WIDTH, height: 64)))
        viewInput.backgroundColor = UIColor.clear
        let button: UIButton = UIButton(type: UIButtonType.custom)
        //set image for button
        button.setImage(#imageLiteral(resourceName: "fb_next"), for: UIControlState.normal)
        button.contentHorizontalAlignment = .right
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 16)
        //set frame
        button.frame = CGRect(origin: CGPoint(x: ScreenSize.SCREEN_WIDTH-84,y :0), size: CGSize(width: 84, height: 64))
        button.addTarget(self, action:#selector(doneButtonAction)  , for: .touchUpInside)
        btnNext.addTarget(self, action:#selector(doneButtonAction)  , for: .touchUpInside)
        
        let buttonBlank: UIButton = UIButton(type: UIButtonType.custom)
        //set image for button
        //set frame
        buttonBlank.frame = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: ScreenSize.SCREEN_WIDTH, height: 64))
        buttonBlank.addTarget(self, action:#selector(blankButtonAction)  , for: .touchUpInside)
        buttonBlank.backgroundColor = UIColor.clear
        
        viewInput.addSubview(buttonBlank)
        
        viewInput.addSubview(button)
        btnBack.addTarget(self, action:#selector(actionBtnBack)  , for: .touchUpInside)
        tfMobileNo?.inputAccessoryView = viewInput
        tfFirstname?.inputAccessoryView = viewInput
        tfLastName?.inputAccessoryView = viewInput
        tfUserName?.inputAccessoryView = viewInput
        tfEmail?.inputAccessoryView = viewInput
        tfPassword?.inputAccessoryView = viewInput
        tfConfirmPassword?.inputAccessoryView = viewInput
        
        changeTextFeildsAttributes(tfFirstname)
        changeTextFeildsAttributes(tfLastName)
        changeTextFeildsAttributes(tfMobileNo)
        changeTextFeildsAttributes(tfUserName)
        changeTextFeildsAttributes(tfEmail)
        changeTextFeildsAttributes(tfPassword)
        changeTextFeildsAttributes(tfConfirmPassword)
      
        tfOTP?.inputAccessoryView = viewInput
        changeTextFeildsAttributes(tfOTP)
    }
    
    
    func changeTextFeildsAttributes(_ textfields : TextField?){
        
        textfields?.placeholderNormalColor = UIColor.white.withAlphaComponent(0.7)
        textfields?.placeholderActiveColor = UIColor.white.withAlphaComponent(0.5)
        textfields?.dividerNormalColor = UIColor.white.withAlphaComponent(0.3)
        textfields?.dividerActiveColor = UIColor.white.withAlphaComponent(0.6)
        textfields?.textColor = UIColor.white
        textfields?.detailColor = UIColor.white.withAlphaComponent(0.5)
        textfields?.font = UIFont.AvenirNextDemiBold(size: 14.0)
        
        textfields?.inputAccessoryView?.isHidden = true
    }
    
    func doneButtonAction(){}
    func blankButtonAction(){}
    
    func actionBtnBack() {
        dismissKeyboard()
        popVC()
    }

}

//MARK: - UITextFieldDelegate

extension LoginDeciderViewController : UITextFieldDelegate {
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        guard let nextTf = view.viewWithTag((textField.tag + 1)) as? UITextField else{
            textField.resignFirstResponder()
            return true
        }
        nextTf.becomeFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text: NSString = (textField.text ?? "") as NSString
        let resultString = text.replacingCharacters(in: range, with: string)
        textField.inputAccessoryView?.isHidden = false
      
//            if case .failure(let msg) = User.validateMobile(mobile: tfMobileNo == textField ? resultString : tfMobileNo?.text){
//                print(msg)
//                textField.inputAccessoryView?.alpha = 0.5
//                
//            }else{
//                textField.inputAccessoryView?.alpha = 1.0
//            }
            
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= 30
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
}



//MARK: - actions


extension LoginDeciderViewController {
    
    
    
    
    
}
