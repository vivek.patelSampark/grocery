//
//  EnterMobileController.swift
//  Tracking
//
//  Created by vivek patel on 18/04/18.
//  Copyright © 2018 vivek patel. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import Material

class EnterMobileController: LoginDeciderViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

        addDoneButtonOnKeyboard()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        ez.runThisAfterDelay(seconds: 0.3) {
            _ =  self.tfMobileNo.becomeFirstResponder()
        }
    }
    
    override func doneButtonAction() {
    
        Toast.show(text: "OTP has been sent to your registered mobile number.", type: .success, layout: .messageView, direction: .top)
        self.performSegue(withIdentifier: "SegueSignUp2", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       arc4random_stir()
        let vc = segue.destination as? OTPController
        vc?.empIDMObile = tfMobileNo.text
        vc?.otp = generateRandomNumber(numDigits: 4).toString
        
    }
    
//    func generateRandomNumber(int numDigits){
//        var place = 1
//        var finalNumber = 0;
//        for(int i = 0; i < numDigits; i++){
//            place *= 10
//            var randomNumber = arc4random_uniform(10)
//            finalNumber += randomNumber * place
//        }
//        return finalNumber
//    }
    
    func generateRandomNumber(numDigits : Int)-> Int{
        
        var place :  UInt32 = 1
        var finalNumber = 0
        
        for _ in 1 ..< numDigits{
            
            place *= 10
            let randomNumber = arc4random_uniform(10)
            finalNumber = Int(finalNumber + (randomNumber * place))
        
        }
        
        return finalNumber
        
    }
    
    
}
