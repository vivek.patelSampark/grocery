//
//  OTPController.swift
//  Tracking
//
//  Created by vivek patel on 18/04/18.
//  Copyright © 2018 vivek patel. All rights reserved.
//

import UIKit
import Material
import EZSwiftExtensions

class OTPController: LoginDeciderViewController {

    var empIDMObile : String?
    var otp : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addDoneButtonOnKeyboard()
        print("abccc" , empIDMObile , otp)
        webServicesOTP()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        ez.runThisAfterDelay(seconds: 0.3) {
            _ =  self.tfOTP.becomeFirstResponder()
        }
    }
    
    override func doneButtonAction() {
        
        if (tfOTP.text ?? "") == (otp ?? ""){
            self.performSegue(withIdentifier: "SegueProfile", sender: self)
        } else {
           Toast.show(text: "Please enter correct OTP", type: .success, layout: .messageView, direction: .top)
        }
    
    }
    
    func webServicesOTP(){
        
        APIManager.shared.request(with: LoginEndPoint.getOTP(mobile: empIDMObile ?? "", message: otp ?? "")) { [unowned self] (response) in
            switch response {
            case .success(let data) :
             //   let data = (data as? ResponseModal)?.status
                
                
                
                print("status" , data)
            case .failure(_): break
            }
        }
    }

}

//MARK: - UITextFieldDelegate

extension OTPController  {
    
    
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        guard let nextTf = view.viewWithTag((textField.tag + 1)) as? UITextField else{
            textField.resignFirstResponder()
            return true
        }
        nextTf.becomeFirstResponder()
        return true
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text: NSString = (textField.text ?? "") as NSString
        let resultString = text.replacingCharacters(in: range, with: string)
        textField.inputAccessoryView?.isHidden = false
        
//        if case .failure(let msg) = User.validateOTP(otp: tfOTP == textField ? resultString : tfOTP?.text){
//            print(msg)
//            textField.inputAccessoryView?.alpha = 0.5
//
//        }else{
//            textField.inputAccessoryView?.alpha = 1.0
//        }
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= 30
        
    }
    
    override func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
}
