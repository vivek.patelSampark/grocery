//
//  SetupProfileViewController.swift
//  Tracking
//
//  Created by vivek patel on 18/04/18.
//  Copyright © 2018 vivek patel. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import Material

class SetupProfileViewController: LoginDeciderViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        addDoneButtonOnKeyboard()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        ez.runThisAfterDelay(seconds: 0.3) {
            _ =  self.tfFirstname.becomeFirstResponder()
        }
    }
    
    override func doneButtonAction() {
        
//        if case .failure(let msg) =   User.validateProfile(firstName: tfFirstname.text, lastName: tfLastName.text, userName: tfUserName.text, email:tfEmail.text, password: tfPassword.text, confirmPass: tfConfirmPassword.text){
//            print(msg)
//
//            Toast.show(text: msg, type: .success, layout: .messageView, direction: .top)
//
//        } else {
//
//            let storyboard = UIStoryboard(name: "Manager", bundle: nil)
//            guard let vc = storyboard.instantiateViewController(withIdentifier: "DashboardController") as? DashboardController else {return }
//            navigationController?.pushViewController(vc, animated: true)
//        }
        
    }
    

}

extension SetupProfileViewController  {
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        guard let nextTf = view.viewWithTag((textField.tag + 1)) as? UITextField else{
            textField.resignFirstResponder()
            return true
        }
        nextTf.becomeFirstResponder()
        return true
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text: NSString = (textField.text ?? "") as NSString
        let resultString = text.replacingCharacters(in: range, with: string)
        textField.inputAccessoryView?.isHidden = false
        
     
//        if case .failure(let msg) =   User.validateProfile(firstName: tfFirstname == textField ? resultString : tfFirstname.text, lastName: tfLastName == textField ? resultString : tfLastName.text, userName: tfUserName == textField ? resultString : tfUserName.text, email: tfEmail == textField ? resultString : tfEmail.text, password: tfPassword == textField ? resultString : tfPassword.text, confirmPass: tfConfirmPassword == textField ? resultString : tfConfirmPassword.text){
//            print(msg)
//            textField.inputAccessoryView?.alpha = 0.5
//
//        }else{
//            textField.inputAccessoryView?.alpha = 1.0
//        }
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= 30
        
    }
    
    override func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
}

